/************************************************************************/
/* moja-elka.blogspot.com                                               */
/* Ultradźwiękowy czujnik odległości na Digispark ATtiny 85             */
/************************************************************************/
#include <NewPing.h>
#include <Adafruit_NeoPixel.h>

//NeoPixels config
#define PIN 0
#define NUMPIXELS 16

//LCD config (R,G,B)
#define GREEN  0,255,0
#define YELLOW 255,255,0
#define ORANGE 255,69,0
#define RED    255,0,0
#define GREEN_THRESHOLD  9
#define YELLOW_THRESHOLD 5
#define ORANGE_THRESHOLD 2
#define NO_ACTIVITY_TIMEOUT 5000  //in miliseconds

//NewPing config
#define TRIGGER_PIN  3
#define ECHO_PIN     4
#define DISTANCE_MAX 100
#define DISTANCE_MIN 30

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
NewPing sonar(TRIGGER_PIN, ECHO_PIN, DISTANCE_MAX);
unsigned long lastPositionTime = 0;
uint8_t distance = 0;
uint8_t lastDistance = 0;
uint8_t forceStandby = 0;

void setup()
{
    pixels.begin();
}

void loop()
{
  distance = sonar.ping_cm();
  
  if (abs(distance - lastDistance) > 5) {
    lastDistance = distance;
    lastPositionTime = millis();
    forceStandby = 0;
  } else if (millis() - lastPositionTime > NO_ACTIVITY_TIMEOUT) {
    //force one blinking LED
    forceStandby = 1;
  }

  if (
    distance > DISTANCE_MAX 
    || 0 == distance 
    || 1 == forceStandby
  ) {
    outOfMeterRange();
  } else {
    inMeterRange(distance);
  }
}

void outOfMeterRange()
{
    pixels.setPixelColor(NUMPIXELS - 1, pixels.Color(GREEN));
    pixels.show();
    delay(500);
    pixels.clear();
    pixels.show();
    delay(500);
}

void inMeterRange(int distance)
{
    int countTo;

    if (distance > DISTANCE_MIN && distance < DISTANCE_MAX) {
      countTo = map(distance, 0, 100, 0, 15);
    } else {
      countTo = 0;
    }
    
    for (int i = NUMPIXELS - 1; i >= countTo; i--) {
        if (i > GREEN_THRESHOLD) {
          pixels.setPixelColor(i, pixels.Color(GREEN));
        } else if (i > YELLOW_THRESHOLD) {
          pixels.setPixelColor(i, pixels.Color(YELLOW));
        } else if (i > ORANGE_THRESHOLD) {
          pixels.setPixelColor(i, pixels.Color(ORANGE));
        } else {
          pixels.setPixelColor(i, pixels.Color(RED));
        }
        
        pixels.show();
        delay(getNextLedDelay(countTo));
    }
    
    delay(200);
    pixels.clear();
    pixels.show();
    delay(500);
}

int getNextLedDelay(int counter)
{
  return 20 + counter * 2;
}
